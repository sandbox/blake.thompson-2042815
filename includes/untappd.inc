<?php
/**
 * @file
 * Default values for search result fields
 */

  $defaults = array(
    'brewery_name' => array(
      'label' => 'Brewery',
      'weight' => -10,
      'enable' => TRUE,
      'show_label' => TRUE,
    ),
    'beer_abv' => array(
      'label' => 'ABV',
      'weight' => -9,
      'enable' => TRUE,
      'show_label' => TRUE,
    ),
    'beer_ibu' => array(
      'label' => 'IBU',
      'weight' => -8,
      'enable' => TRUE,
      'show_label' => TRUE,
    ),
    'beer_style' => array(
      'label' => 'Style',
      'weight' => -7,
      'enable' => TRUE,
      'show_label' => TRUE,
    ),
    'beer_description' => array(
      'label' => 'Description',
      'weight' => -6,
      'enable' => TRUE,
      'show_label' => FALSE,
    ),
    'brewery_label' => array(
      'label' => 'Brewery Label',
      'weight' => 0,
      'enable' => FALSE,
      'show_label' => FALSE,
    ),
    'beer_label' => array(
      'label' => 'Beer Label',
      'weight' => 0,
      'enable' => FALSE,
      'show_label' => FALSE,
    ),
    'country_name' => array(
      'label' => 'Country',
      'weight' => 0,
      'enable' => FALSE,
      'show_label' => FALSE,
    ),
    'brewery_city' => array(
      'label' => 'City',
      'weight' => 0,
      'enable' => FALSE,
      'show_label' => FALSE,
    ),
    'brewery_state' => array(
      'label' => 'State',
      'weight' => 0,
      'enable' => FALSE,
      'show_label' => FALSE,
    ),
    'checkin_count' => array(
      'label' => 'Checkin Count',
      'weight' => 0,
      'enable' => FALSE,
      'show_label' => TRUE,
    ),
    'your_count' => array(
      'label' => 'Your Count',
      'weight' => 0,
      'enable' => FALSE,
      'show_label' => TRUE,
    ),
  );
