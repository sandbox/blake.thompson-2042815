README.txt
----------

Third-party integration module that brings Untappd search and checkin
functionality to Drupal.

 It also offers:
 - an Untappd search block with optional filters
 - a block for connecting with Untappd oauth
 - customizable Untappd search results


Author
------
Blake Thompson
<b.lake at outlook DOT com>
