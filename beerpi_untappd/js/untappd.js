(function($) {
	Drupal.behaviors.checkin = {
		attach: function() {
      var social = $.cookie('utsocial');
      if (social) {
        $('form[id^=beerpi-untappd-checkin-form] .form-type-checkbox input').each(function() {
          $this = $(this);
          if (social.indexOf($this.attr('name').replace('untappd-','')) === -1) {
            $this.parent('div').css('visibility', 'hidden');
          }
        });
      }
			$('.checkin-button').click(function() {
        $this = $(this);
        $this.next('form').show(250);
        $this.hide();
        // TODO: For modal
        //$("#untappd-checkin").show();
        /*var maskHeight = $(document).height();
        var modalTop = $(window).height()/2 + $(window).scrollTop() - ($("#untappd-checkin").height()/2);
        var modalLeft = $(window).width()/2 - ($("#untappd-checkin").width()/2);
        $("#untappd-mask").show().css('height', maskHeight);
        $("#untappd-checkin").css('top', modalTop + 'px');
        $("#untappd-checkin").css('left', modalLeft + 'px');
        $("#untappd-bid").val($(this).attr('href').replace('#',''));*/
      });
      
      $('[name="untappd-shout"]').keyup(function() {
        $this = $(this);
        $formId = $this.parents('form').attr('id');
        $charCount = $('#' + $formId + ' .untappd-char-count');
        $charCount.html($this.val().length + '/' + '140');
        if ($this.val().length > 140 && $charCount.css('color') != 'rgb(255, 0, 0)') {
          $charCount.css('color', 'red');
        }
        if ($this.val().length <= 140 && $charCount.css('color') != 'rgb(0, 0, 0)') {
          $charCount.css('color', '#000000');
        }
      });
      $('#untappd-mask, form[id^=beerpi-untappd-checkin-form] input[id^="edit-untappd-cancel"]').click(function() {
        $this = $(this);
        $formId = $this.parents('form').attr('id');
        $('#' + $formId).hide();
        $('#' + $formId).prev('input').show();
        $('#' + $formId + '>div').children().show();
        $('#' + $formId + ' .untappd-response').html('');
        $('#' + $formId + ' input[id^="edit-untappd-cancel"]').val('Cancel');
        $('#' + $formId + ' input[id^="edit-untappd-confirm"]').show();
        $('#untappd-mask').hide();
      });
      $('form[id^=beerpi-untappd-checkin-form] input[value="Confirm"]').click(function() {
        $this = $(this);
        $formId = $this.parents('form').attr('id');
        if ($('#' + $formId + ' [name="untappd-shout"]').val().length > 140) {
          alert('You\'re over the 140 character limit.');
          return false;
        } else {
          var url = 'http://api.untappd.com/v4/checkin/add?access_token=' + $.cookie('utt');
          var d = new Date()
          var n = parseInt(d.getTimezoneOffset())/60;
          var gmt_offset = n * -1;
          var timezone = d.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/,'$1');
          var params = {
            'gmt_offset' : gmt_offset,
            'timezone' : timezone,
            'twitter' : $('#' + $formId + ' [name="untappd-twitter"]').attr('checked') == true ? 'on' : 'off',
            'facebook' : $('#' + $formId + ' [name="untappd-facebook"]').attr('checked') == true ? 'on' : 'off',
            'shout' : escape($('#' + $formId + ' [name="untappd-shout"]').val()),
            'bid' : $('#' + $formId + ' [name="untappd-bid"]').val()
          }
          if ($('#' + $formId + ' [name="untappd-rate"]').val() > 0) {
            params.rating = $('#' + $formId + ' [name="untappd-rate"]').val();
          }
          $this.next('.untappd-throbber').css('display','inline-block');
          $.ajax({
            url : url,
            type : 'POST',
            dataType : 'json',
            data : params,
            success : function(data) {
              console.log(data);
              if (data.meta.code == 200) {
                var message = '<b>SUCCESS!</b><br/>';
                message += 'This is your ' + data.response.stats.beer + ' ' + data.response.beer.beer_name + ', ' + data.response.stats.beer_month + ' this month';
                data.response.badges.count = 2;
                if (data.response.badge_valid == true && data.response.badges.count > 0) {
                  message += '<div class="untappd-badges">';
                  for (var i = 0; i < data.response.badges.count; i++) {
                    data.response.badges.items[i] = {"badge_image" : {}};
                    data.response.badges.items[i].badge_image.md = 'https://untappd.s3.amazonaws.com/assets/badges/bdg_July4th2013_md.png';
                    data.response.badges.items[i].badge_name = 'Badge Name';
                    message += '<div class="untappd-badge">';
                    message += '<img src="' + data.response.badges.items[i].badge_image.md + '" /><br/>';
                    message += data.response.badges.items[i].badge_name;
                    message += '</div>';
                  }
                  message += '</div>';
                }
                $('#' + $formId + '>div').children().not(".untappd-buttons, .untappd-response").hide();
                $('#' + $formId + ' .untappd-response').html(message);
                $('#' + $formId + ' [name="untappd-shout"]').val('');
                $('#' + $formId + ' [name="untappd-rate"]').val('0');
                $('#' + $formId + ' .untappd-char-count').html('0/140');
                $('#' + $formId + ' [name="untappd-twitter"]').attr('checked', false);
                $('#' + $formId + ' [name="untappd-facebook"]').attr('checked', false);
                $this.next('.untappd-throbber').hide();
                $this.prev('[value="Cancel"]').val('Done');
                $this.hide();
              }
            },
            error : function(error) {
              console.log(error);
              var message = 'Oops! ';
              if (error.response.error_detail.length > 1) {
                message += error.response.error_detail;
              } else {
                message += "Something went wrong.";
              }
              $('#' + $formId + ' .untappd-response').html(message);
              $this.next('.untappd-throbber').hide();
            },
          });
          console.log(params);
        }
      });
		}
	};
})(jQuery);
