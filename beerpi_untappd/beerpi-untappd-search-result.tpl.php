<?php

/**
 * @file
 * Theme individual search results for BeerPI Untappd.
 */
?>
<h2><a href="<?php print $result['link']; ?>"><?php print $result['title']; ?></a></h2>
<div class="search-result">
  <?php print $result['snippet']; ?>
  <?php drupal_render(drupal_get_form('beerpi_untappd_checkin_form', $result['bid'])); ?>
</div>