<?php

/**
 * @file
 * Theme individual search result snippets for BeerPI Untappd.
 */
?>
<?php dpm($snippet);
foreach ($snippet as $field_name => $field) : ?>
  <div class="untappd-result-field <?php print $field_name; ?>-field">
    <?php if ($field['enable'] && !empty($field['value'])) : ?>
      <?php if ($field['show_label']): ?>
        <div class="<?php print $field_name; ?>-label untappd-label"  style="font-weight:bold;">
          <?php print $field['label']; ?>:
        </div>
      <?php endif; ?>
      <div class="<?php print $field_name; ?>-value untappd-value">
        <?php if ($field_name == 'beer_abv') : ?>
          <?php print $field['value'] . '%' ?>
        <?php elseif ($field_name == 'brewery_label' || $field_name == 'beer_label') : ?>
          <img src="<?php print $field['value']; ?>"/>
        <?php else : ?>
          <?php print $field['value']; ?>
        <?php endif; ?>
      </div>
    <?php endif; ?>        
  </div>
<?php endforeach; ?>
<?php if (isset($_COOKIE['utt']) && variable_get('untappd_user_checkin', 0)) : ?> 
  <br/>
  <input type="button" class="form-submit checkin-button" onclick="return false;" value="Checkin"/>
<?php endif; ?>